import os
import time
import random
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

from config import Configuration

config = Configuration()



class Instabot:
    logged_in = False
    browser = None
    photos_urls = []
    userpages_urls = []
    userposts_urls = []

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.browser = webdriver.Chrome(ChromeDriverManager().install())

    @staticmethod
    def __sleep(seconds=None):
        if seconds is None:
            time.sleep(random.randrange(2, 5))
        else:
            time.sleep(seconds)

    def __close_browser(self):
        if self.browser:
            self.browser.close()
            self.browser.quit()

    def __load_browser_url(self, url):
        try:
            self.browser.get(url)
            self.__sleep(config.timeout_load)
        except Exception as e:
            print(f'{e}')

    def login(self):
        self.logged_in = False

        if self.browser is None:
            return False

        try:
            self.__load_browser_url(config.instagram_url)

            username_input = self.browser.find_element_by_name('username')
            username_input.clear()
            username_input.send_keys(config.username)
            print('enter username')

            self.__sleep(config.timeout_input_fill)

            password_input = self.browser.find_element_by_name('password')
            password_input.clear()
            password_input.send_keys(config.password)
            print('enter password')

            self.__sleep(config.timeout_input_fill)

            password_input.send_keys(Keys.ENTER)
            print('signin')

            self.__sleep()

            self.logged_in = True
        except Exception as e:
            print(f'{e}')
            self.__close_browser()

    def __xpath_exist(self, url):
        if self.logged_in and self.browser is not None:
            try:
                self.browser.find_element_by_xpath(url)
                return True
            except NoSuchElementException:
                return False

    def __scroll_browser_down(self, pages=None):
        scroll_range = pages if pages is not None else config.range_scroll
        for i in range(1, scroll_range):
            print(f'scroll browser down step: {i}')
            self.browser.execute_script(
                'window.scrollTo(0, document.body.scrollHeight);'
            )
            self.__sleep(config.timeout_scroll)

    def collect_photo_links_by_hashtag(self, hashtag):
        if self.logged_in and self.browser is not None:
            # load url
            self.__load_browser_url(f'https://www.instagram.com/explore/tags/{hashtag}/')

            # scroll down
            self.__scroll_browser_down()

            # collect all hrefs
            hrefs = self.browser.find_elements_by_tag_name('a')
            post_hrefs = [item.get_attribute('href') for item in hrefs if '/p/' in item.get_attribute('href')]
            print(f'collect photo links: {len(post_hrefs)}')

            return post_hrefs
        return []

    def like_photo_by_hashtag(self, hashtag):
        post_hrefs = self.collect_photo_links_by_hashtag(hashtag)

        for url in post_hrefs[0:config.range_like]:
            print(f'process photo by url: {url}')
            try:
                self.__load_browser_url(url)

                # like photo

                if self.__xpath_exist(config.xpath_selector_like_button):
                    like_button = self.browser.find_element_by_xpath(
                        config.xpath_selector_like_button
                    ).click()
                    print('put like')
                    self.photos_urls.append(url)
                else:
                    print('Cant find Like button')

                # get userpage href
                userpage_link = self.browser.find_element_by_xpath(config.xpath_userpage_link)
                userpage_href = userpage_link.get_attribute('href')

                self.userpages_urls.append(userpage_href)
                print(f'append userpage: {userpage_href}')

                self.__sleep(config.timeout_like)
            except Exception as e:
                print(f'{e}')

    def collect_userpage_posts_links(self, userpage):
        self.__load_browser_url(userpage)
        try:
            print(f'process userpage: {userpage}')

            # get user publications counter
            userpage_publications_counter_element = self.browser.find_element_by_xpath(config.xpath_userpage_publication_counter)
            userpage_publications_count = int(userpage_publications_counter_element.text)
            print(f'found {userpage_publications_count} user publications')
            if config.range_user_publications is None:
                count_of_scrolls = int(userpage_publications_count / 12)
            else:
                count_of_scrolls = 3
                print('Mock count_of_scrolls')

            # scroll down
            self.__scroll_browser_down(pages=count_of_scrolls)

            # collect hrefs
            hrefs = self.browser.find_elements_by_tag_name('a')
            post_hrefs = [item.get_attribute('href') for item in hrefs if '/p/' in item.get_attribute('href')]
            print(f'collect userpage posts links: {len(post_hrefs)}')

            self.userposts_urls += post_hrefs

            # follow user
            self.__follow_user()
        except Exception as e:
            print(f'{e}')

    def __follow_user(self):
        if config.follow_every_user:
            self.browser.find_element_by_xpath(config.xpath_userpage_follow_button).click()

    def like_post_by_link(self, post_link):
        print(f'process post: {post_link}')
        self.__load_browser_url(post_link)

        try:
            if self.__xpath_exist(config.xpath_selector_like_button):
                like_button = self.browser.find_element_by_xpath(
                    config.xpath_selector_like_button
                ).click()
                print('put like')
        except Exception as e:
            print(f'{e}')

    def run(self, hashtags):
        if not isinstance(hashtags, list):
            hashtags = [hashtags]

        # like photos and collect userpage links
        for hashtag in hashtags:
            print(f'process hashtag: {hashtag}')
            self.like_photo_by_hashtag(hashtag=hashtag)

        # scan usernages and collect user posts links
        for userpage in self.userpages_urls:
            self.collect_userpage_posts_links(userpage)

        # scan posts and like
        for post_link in self.userposts_urls[0:config.range_like]:
            self.like_post_by_link(post_link=post_link)

        self.__close_browser()


hashtags = ['pythons']

insta = Instabot(username=config.username, password=config.password)
insta.login()
insta.run(hashtags)
print('Complete!!!')